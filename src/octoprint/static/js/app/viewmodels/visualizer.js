$(function() {
    function VisualizerViewModel(parameters) {
        var self = this;
       
	self.state = parameters[1];       
	
	self.file = ko.observable(undefined);
	
	self.file = self.state.filename;

    self.webcam_view = ko.observable(false); 	
    self.gcode_view = ko.observable(true); 	
	
	self.webcam_flipH = ko.computed(function() {
 		return parameters[0].webcam_flipH();
  	}); 
	self.webcam_flipV = ko.computed(function() {
 		return parameters[0].webcam_flipV();
  	}); 
	self.isFileLoaded = ko.computed(function(){
	  if (ko.toJS(self.file) === "undefined" || ko.toJS(self.file) === null )
	  	return false;
	  if(scene !== null){
	  	console.log(scene.children.slice(-1).pop().name);
	  	console.log(ko.toJS(self.file));
		if (scene.children.slice(-1).pop().name !== ko.toJS(self.file)) 
			return true;
		else
			return false;
	  }
	  return true;
	});

	self.toggle_webcam_view = function() {
		self.webcam_view(true);
		self.gcode_view(false);
		}
	self.toggle_gcode_view = function() {
		self.webcam_view(false);
		self.gcode_view(true);
		}

	
	// openGCodeFromPath(ko.toJS(self.state.filename));
   	}


 

    OCTOPRINT_VIEWMODELS.push([
        VisualizerViewModel,
        ["settingsViewModel","printerStateViewModel"],
        "#visualizer_wrapper"
    ]);
});
