.. _sec-development-interface-filemanager:

``octoprint.filemanager``
-------------------------

.. automodule:: octoprint.filemanager
   :members:
   :undoc-members:

.. _sec-development-interface-filemanager-analysis:

``octoprint.filemanager.analysis``
----------------------------------

.. automodule:: octoprint.filemanager.analysis
   :members:
   :undoc-members:

.. _sec-development-interface-filemanager-destinations:

``octoprint.filemanager.destinations``
--------------------------------------

.. automodule:: octoprint.filemanager.destinations
   :members:
   :undoc-members:

.. _sec-development-interface-filemanager-storage:

``octoprint.filemanager.storage``
---------------------------------

.. automodule:: octoprint.filemanager.storage
   :members: StorageInterface, LocalFileStorage

