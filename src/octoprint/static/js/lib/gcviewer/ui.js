
var scene = null;
var object = null;

function openGCodeFromText(gcode,name) {
  if(object !== null)
    remove_object()
  object = createObjectFromGCode(gcode);
  object.name = name;
  scene.add(object);
}
function remove_object(){
    var selectedObject = scene.getObjectByName(object.name);
    scene.remove( selectedObject );
}
function render_3dpreview(file_loaded) {

      file_loaded = $("#file_loaded").html();

      if(!file_loaded)
        return false;

      var loadFile = $.ajax({
            url: BASEURL + "downloads/files/local/" + file_loaded,
            type: "GET",
            context: document.body,
	          dataType: "json",
            crossDomain: "true",
            global: false,
            async:false,
            success: function(response, rstatus) {
                if(rstatus === 'success'){
                   return response;
                }
            },
            error: function() {
                self.status = "idle";
                self.errorCount++;
            }
        }).responseText;

    if(!scene)
      scene = createScene($('#renderArea'));

    openGCodeFromText( loadFile , file_loaded );

    $(".load_file_button").hide();
}

$(function() {


  if (!Modernizr.webgl) {
    alert('Sorry, you need a WebGL capable browser to use this.\n\nGet the latest Chrome or FireFox.');
    return;
  }

  if (!Modernizr.localstorage) {
    alert("Man, your browser is ancient. I can't work with this. Please upgrade.");
    return;
  }

});

